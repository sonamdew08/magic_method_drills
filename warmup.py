#Add two integers using a suitable magic method
num1 = 10
num2 = 2
print(num1.__add__(num2))

# Find the length of a list using a suitable magic method
list1 = [1, 2, 3, 4, 5, 6, 7]
print(list1.__len__())

# Find the length of a string using a suitable magic method
name = "Sonam Dewangan" 
print(name.__len__())

# Add two lists by calling a magic method on the first list
list2 = [9, 8, 7, 6, 5, 4]
print(list1.__add__(list2))