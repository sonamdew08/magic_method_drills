import random

class BankAccount:
    
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance
        self.account_balance =[]

    def __repr__(self):
        self.account_balance.append(self.balance)
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)

    def __eq__(self, other):
        return other.balance == self.balance and other.id == self.id
  
    def __lt__(self, other):
        return self.balance < other.balance

class Bank(BankAccount):
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __iter__(self):  
        for i in self.accounts:
            yield i

    
        
bank = Bank()
for i in range(100):
    bank.add_account(BankAccount(id=i, balance=i * 100))

for account in bank:
    print(account)


accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
print(sorted(accounts))
a1 = BankAccount(id=1, balance=100)
a2 = BankAccount(id=1, balance=100)
a3 = BankAccount(id=2, balance=100)
print(a1 == a2)
print(a2 == a3)



